<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function(){
    return response("Welcome",200);
});

$router->get('/content/', 'ContentController@getContent');
$router->get('/content/{selector}', 'ContentController@getContent');


$router->post(
    'auth/login', 
    [
       'uses' => 'AuthController@authenticate'
    ]
);
$router->post(
    'auth/register', 
    [
       'uses' => 'AuthController@register'
    ]
);

$router->group(
    ['middleware' => 'jwt.auth'], 
    function() use ($router) {

        $router->get('users', function() {
            $users = \App\User::all();
            return response()->json($users);
        });

        $router->post('/content','ContentController@saveContent');
        $router->put('/content/{id}','ContentController@saveContent');
        $router->delete('/content/{id}','ContentController@deleteContent');
    }
);