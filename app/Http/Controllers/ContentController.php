<?php

namespace App\Http\Controllers;

use App\Content;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Lumen\Routing\Controller as BaseController;


class ContentController extends BaseController
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }


    public function getContent(string $selector = null)
    {
        if(is_null($selector)){
            $content = Content::all();
        }else if(is_int($selector)){
            $content = Content::findOrFail($selector);
        }else{
            $content = Content::where("type", $selector)->get();
        }
        
        return response()->json([
            "content" => $content
        ], 201);
    }
    public function saveContent(int $id = null)
    {
        $data = $this->request->json()->all();
       
        $rules = [
            'title' => 'required|min:4',
            'url'  => 'required|url',
            'type' => 'required',
            'status' => Rule::requiredIf(function () use ($data) {
                return (isset($data['type']) && (in_array($data['type'],['page','post'])));
            }),
            'order' => Rule::requiredIf(function () use ($data) {
                return (isset($data['type']) && (in_array($data['type'],['menu_item'])));                   
            }),
        ];
        
        $validator = Validator::make($data, $rules);
        
        if($validator->fails()){
            return response($validator->errors()->all());
        }
        
        if(is_int($id)){
            $content = Content::findOrFail($id);
            $content->parent_id = $data['parent_id']; 
            $content->title = $data['title'];
            $content->url = $data['url'];
            $content->order = $data['order'];
            $content->type = $data['type'];
            $content->text = $data['text'];
            $content->status = $data['status'];   

            $content->save(); 
        }else{
            Content::create($data);
        }
        return response('Success', 201)->header('Content-Type', 'text/plain');
    }

    public function deleteContent(int $id)
    {
        $content = Content::findOrFail($id);
        $content->delete();
        return response('Success', 200)->header('Content-Type', 'text/plain');
    }
       
}
