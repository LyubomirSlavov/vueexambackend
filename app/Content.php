<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model{

  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'parent_id', 'title', 'url', 'order', 'type', 'text', 'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}

